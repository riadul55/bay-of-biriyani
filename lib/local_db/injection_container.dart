import 'package:get_it/get_it.dart';

import 'repositories/db_repositories.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerLazySingleton<DbRepositories>(
        () => DbRepositories(),
  );
}