import 'package:moor_flutter/moor_flutter.dart';

import 'tables/db_foods.dart';

part 'AppDb.g.dart';

@UseMoor(
  tables: [DBFoods],
  queries: {
    'fetchAllCarts' : 'SELECT * FROM FoodsTable',
    'isCartAdded' : 'SELECT * FROM FoodsTable WHERE food_id = ?',
    'updateCartById' : 'UPDATE FoodsTable SET quantity = ? where id = ?',
    'deleteCartById' : 'Delete FROM FoodsTable WHERE food_id = ?',
    'deleteAllCarts' : 'Delete FROM FoodsTable',
  }
)

//Don't delete this command => flutter packages pub run build_runner watch
class AppDb extends _$AppDb {
  AppDb() : super(FlutterQueryExecutor.inDatabaseFolder(path: 'local-db'));

  @override
  int get schemaVersion => 1;
}
