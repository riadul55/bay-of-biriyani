import 'package:moor_flutter/moor_flutter.dart';

@DataClassName("FoodsTable")
class DBFoods extends Table {
  IntColumn get id => integer().autoIncrement()();

  IntColumn get foodId => integer()();

  TextColumn get name => text()();
  TextColumn get image => text()();
  TextColumn get price => text()();
  TextColumn get charge => text()();

  IntColumn get quantity => integer()();

  // @override
  // Set<Column> get primaryKey => {id};

  @override
  String get tableName => "FoodsTable";
}