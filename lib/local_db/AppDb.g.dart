// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppDb.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class FoodsTable extends DataClass implements Insertable<FoodsTable> {
  final int id;
  final int foodId;
  final String name;
  final String image;
  final String price;
  final String charge;
  final int quantity;
  FoodsTable(
      {@required this.id,
      @required this.foodId,
      @required this.name,
      @required this.image,
      @required this.price,
      @required this.charge,
      @required this.quantity});
  factory FoodsTable.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return FoodsTable(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      foodId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}food_id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      image:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}image']),
      price:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}price']),
      charge:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}charge']),
      quantity:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}quantity']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || foodId != null) {
      map['food_id'] = Variable<int>(foodId);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || image != null) {
      map['image'] = Variable<String>(image);
    }
    if (!nullToAbsent || price != null) {
      map['price'] = Variable<String>(price);
    }
    if (!nullToAbsent || charge != null) {
      map['charge'] = Variable<String>(charge);
    }
    if (!nullToAbsent || quantity != null) {
      map['quantity'] = Variable<int>(quantity);
    }
    return map;
  }

  DBFoodsCompanion toCompanion(bool nullToAbsent) {
    return DBFoodsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      foodId:
          foodId == null && nullToAbsent ? const Value.absent() : Value(foodId),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      image:
          image == null && nullToAbsent ? const Value.absent() : Value(image),
      price:
          price == null && nullToAbsent ? const Value.absent() : Value(price),
      charge:
          charge == null && nullToAbsent ? const Value.absent() : Value(charge),
      quantity: quantity == null && nullToAbsent
          ? const Value.absent()
          : Value(quantity),
    );
  }

  factory FoodsTable.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return FoodsTable(
      id: serializer.fromJson<int>(json['id']),
      foodId: serializer.fromJson<int>(json['foodId']),
      name: serializer.fromJson<String>(json['name']),
      image: serializer.fromJson<String>(json['image']),
      price: serializer.fromJson<String>(json['price']),
      charge: serializer.fromJson<String>(json['charge']),
      quantity: serializer.fromJson<int>(json['quantity']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'foodId': serializer.toJson<int>(foodId),
      'name': serializer.toJson<String>(name),
      'image': serializer.toJson<String>(image),
      'price': serializer.toJson<String>(price),
      'charge': serializer.toJson<String>(charge),
      'quantity': serializer.toJson<int>(quantity),
    };
  }

  FoodsTable copyWith(
          {int id,
          int foodId,
          String name,
          String image,
          String price,
          String charge,
          int quantity}) =>
      FoodsTable(
        id: id ?? this.id,
        foodId: foodId ?? this.foodId,
        name: name ?? this.name,
        image: image ?? this.image,
        price: price ?? this.price,
        charge: charge ?? this.charge,
        quantity: quantity ?? this.quantity,
      );
  @override
  String toString() {
    return (StringBuffer('FoodsTable(')
          ..write('id: $id, ')
          ..write('foodId: $foodId, ')
          ..write('name: $name, ')
          ..write('image: $image, ')
          ..write('price: $price, ')
          ..write('charge: $charge, ')
          ..write('quantity: $quantity')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          foodId.hashCode,
          $mrjc(
              name.hashCode,
              $mrjc(
                  image.hashCode,
                  $mrjc(price.hashCode,
                      $mrjc(charge.hashCode, quantity.hashCode)))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is FoodsTable &&
          other.id == this.id &&
          other.foodId == this.foodId &&
          other.name == this.name &&
          other.image == this.image &&
          other.price == this.price &&
          other.charge == this.charge &&
          other.quantity == this.quantity);
}

class DBFoodsCompanion extends UpdateCompanion<FoodsTable> {
  final Value<int> id;
  final Value<int> foodId;
  final Value<String> name;
  final Value<String> image;
  final Value<String> price;
  final Value<String> charge;
  final Value<int> quantity;
  const DBFoodsCompanion({
    this.id = const Value.absent(),
    this.foodId = const Value.absent(),
    this.name = const Value.absent(),
    this.image = const Value.absent(),
    this.price = const Value.absent(),
    this.charge = const Value.absent(),
    this.quantity = const Value.absent(),
  });
  DBFoodsCompanion.insert({
    this.id = const Value.absent(),
    @required int foodId,
    @required String name,
    @required String image,
    @required String price,
    @required String charge,
    @required int quantity,
  })  : foodId = Value(foodId),
        name = Value(name),
        image = Value(image),
        price = Value(price),
        charge = Value(charge),
        quantity = Value(quantity);
  static Insertable<FoodsTable> custom({
    Expression<int> id,
    Expression<int> foodId,
    Expression<String> name,
    Expression<String> image,
    Expression<String> price,
    Expression<String> charge,
    Expression<int> quantity,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (foodId != null) 'food_id': foodId,
      if (name != null) 'name': name,
      if (image != null) 'image': image,
      if (price != null) 'price': price,
      if (charge != null) 'charge': charge,
      if (quantity != null) 'quantity': quantity,
    });
  }

  DBFoodsCompanion copyWith(
      {Value<int> id,
      Value<int> foodId,
      Value<String> name,
      Value<String> image,
      Value<String> price,
      Value<String> charge,
      Value<int> quantity}) {
    return DBFoodsCompanion(
      id: id ?? this.id,
      foodId: foodId ?? this.foodId,
      name: name ?? this.name,
      image: image ?? this.image,
      price: price ?? this.price,
      charge: charge ?? this.charge,
      quantity: quantity ?? this.quantity,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (foodId.present) {
      map['food_id'] = Variable<int>(foodId.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (image.present) {
      map['image'] = Variable<String>(image.value);
    }
    if (price.present) {
      map['price'] = Variable<String>(price.value);
    }
    if (charge.present) {
      map['charge'] = Variable<String>(charge.value);
    }
    if (quantity.present) {
      map['quantity'] = Variable<int>(quantity.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DBFoodsCompanion(')
          ..write('id: $id, ')
          ..write('foodId: $foodId, ')
          ..write('name: $name, ')
          ..write('image: $image, ')
          ..write('price: $price, ')
          ..write('charge: $charge, ')
          ..write('quantity: $quantity')
          ..write(')'))
        .toString();
  }
}

class $DBFoodsTable extends DBFoods with TableInfo<$DBFoodsTable, FoodsTable> {
  final GeneratedDatabase _db;
  final String _alias;
  $DBFoodsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _foodIdMeta = const VerificationMeta('foodId');
  GeneratedIntColumn _foodId;
  @override
  GeneratedIntColumn get foodId => _foodId ??= _constructFoodId();
  GeneratedIntColumn _constructFoodId() {
    return GeneratedIntColumn(
      'food_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _imageMeta = const VerificationMeta('image');
  GeneratedTextColumn _image;
  @override
  GeneratedTextColumn get image => _image ??= _constructImage();
  GeneratedTextColumn _constructImage() {
    return GeneratedTextColumn(
      'image',
      $tableName,
      false,
    );
  }

  final VerificationMeta _priceMeta = const VerificationMeta('price');
  GeneratedTextColumn _price;
  @override
  GeneratedTextColumn get price => _price ??= _constructPrice();
  GeneratedTextColumn _constructPrice() {
    return GeneratedTextColumn(
      'price',
      $tableName,
      false,
    );
  }

  final VerificationMeta _chargeMeta = const VerificationMeta('charge');
  GeneratedTextColumn _charge;
  @override
  GeneratedTextColumn get charge => _charge ??= _constructCharge();
  GeneratedTextColumn _constructCharge() {
    return GeneratedTextColumn(
      'charge',
      $tableName,
      false,
    );
  }

  final VerificationMeta _quantityMeta = const VerificationMeta('quantity');
  GeneratedIntColumn _quantity;
  @override
  GeneratedIntColumn get quantity => _quantity ??= _constructQuantity();
  GeneratedIntColumn _constructQuantity() {
    return GeneratedIntColumn(
      'quantity',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, foodId, name, image, price, charge, quantity];
  @override
  $DBFoodsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'FoodsTable';
  @override
  final String actualTableName = 'FoodsTable';
  @override
  VerificationContext validateIntegrity(Insertable<FoodsTable> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('food_id')) {
      context.handle(_foodIdMeta,
          foodId.isAcceptableOrUnknown(data['food_id'], _foodIdMeta));
    } else if (isInserting) {
      context.missing(_foodIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image'], _imageMeta));
    } else if (isInserting) {
      context.missing(_imageMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price'], _priceMeta));
    } else if (isInserting) {
      context.missing(_priceMeta);
    }
    if (data.containsKey('charge')) {
      context.handle(_chargeMeta,
          charge.isAcceptableOrUnknown(data['charge'], _chargeMeta));
    } else if (isInserting) {
      context.missing(_chargeMeta);
    }
    if (data.containsKey('quantity')) {
      context.handle(_quantityMeta,
          quantity.isAcceptableOrUnknown(data['quantity'], _quantityMeta));
    } else if (isInserting) {
      context.missing(_quantityMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FoodsTable map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return FoodsTable.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $DBFoodsTable createAlias(String alias) {
    return $DBFoodsTable(_db, alias);
  }
}

abstract class _$AppDb extends GeneratedDatabase {
  _$AppDb(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $DBFoodsTable _dBFoods;
  $DBFoodsTable get dBFoods => _dBFoods ??= $DBFoodsTable(this);
  Selectable<FoodsTable> fetchAllCarts() {
    return customSelect('SELECT * FROM FoodsTable',
        variables: [], readsFrom: {dBFoods}).map(dBFoods.mapFromRow);
  }

  Selectable<FoodsTable> isCartAdded(int var1) {
    return customSelect('SELECT * FROM FoodsTable WHERE food_id = ?',
        variables: [Variable<int>(var1)],
        readsFrom: {dBFoods}).map(dBFoods.mapFromRow);
  }

  Future<int> updateCartById(int var1, int var2) {
    return customUpdate(
      'UPDATE FoodsTable SET quantity = ? where id = ?',
      variables: [Variable<int>(var1), Variable<int>(var2)],
      updates: {dBFoods},
      updateKind: UpdateKind.update,
    );
  }

  Future<int> deleteCartById(int var1) {
    return customUpdate(
      'Delete FROM FoodsTable WHERE food_id = ?',
      variables: [Variable<int>(var1)],
      updates: {dBFoods},
      updateKind: UpdateKind.delete,
    );
  }

  Future<int> deleteAllCarts() {
    return customUpdate(
      'Delete FROM FoodsTable',
      variables: [],
      updates: {dBFoods},
      updateKind: UpdateKind.delete,
    );
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [dBFoods];
}
