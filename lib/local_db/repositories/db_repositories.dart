import 'package:bay_of_biriyani/local_db/AppDb.dart';
import 'package:moor_flutter/moor_flutter.dart';

class DbRepositories {
  AppDb _appDb = AppDb();

  Future<List<FoodsTable>> getAllCarts() async => _appDb.fetchAllCarts().get();

  Future<FoodsTable> isCartAdded(foodId) async => _appDb.isCartAdded(foodId).getSingleOrNull();

  Future<void> addCart(FoodsTable table) async => await _appDb
      .into(_appDb.dBFoods)
      .insert(table, mode: InsertMode.insertOrReplace);

  Future<int> deleteCartById(foodId) async => await _appDb.deleteCartById(foodId);

  Future<int> updateCartById(foodId, quantity) async => _appDb.updateCartById(quantity, foodId);

  Future<int> deleteAllCarts() async => _appDb.deleteAllCarts();
}