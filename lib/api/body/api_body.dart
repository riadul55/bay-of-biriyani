import 'dart:convert';

import 'package:bay_of_biriyani/utils/constants.dart';

class ApiBody {

  static Map<String, String> getHomeBody({lat, lon}) => {
    "app_secret_key": "${Constants.APP_SECRETE_KEY}",
    "lat" : "$lat",
    "lon" : "$lon"
  };

  static Map<String, String> getItemDetailsBody({ itemId}) =>
      {"item_id": "$itemId", "app_secret_key": "${Constants.APP_SECRETE_KEY}"};

  static Map<String, String> getPromoBody({ promoCode, minSpend}) => {
    "app_secret_key": "${Constants.APP_SECRETE_KEY}",
    "promo_code": "$promoCode",
    "min_spend": "$minSpend"
  };

  static Map<String, String> getOrderHistoryBody({ phone}) =>
      {"phone": "$phone", "app_secret_key": "${Constants.APP_SECRETE_KEY}"};

  static Map<String, String> getOtpVerifyBody({ phoneNumber, type, otp}) =>
      {
        "phone_number": "$phoneNumber",
        "type": "$type",
        "otp": "$otp",
        "app_secret_key": "${Constants.APP_SECRETE_KEY}"
      };

  static Map<String, String> getOrderSaveBody(
      {
        phoneNumber,
        fullName,
        address,
        orderItems,
        deliveryCharge,
        promoValue,
        promoCode,
        paymentType}) =>
      {
        "app_secret_key": "${Constants.APP_SECRETE_KEY}",
        "phone_number": "$phoneNumber",
        "full_name": "$fullName",
        "address": "$address",
        "order_items": "$orderItems",
        "delivery_charge": "$deliveryCharge",
        if (promoValue != null) "promo_value": "$promoValue",
        if (promoCode != null) "promo_code": "$promoCode",
        "payment_type": "$paymentType"
      };

  static Map<String, String> getLoginBody({email, password}) => {
    "email": "$email",
    "password": "$password",
    "app_secret_key": "${Constants.APP_SECRETE_KEY}"
  };

}