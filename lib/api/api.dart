import 'dart:convert';

import 'package:bay_of_biriyani/api/data_models/home_data_model.dart';
import 'package:bay_of_biriyani/api/data_models/item_details_data_model.dart';
import 'package:bay_of_biriyani/api/data_models/order_history_data_model.dart';
import 'package:bay_of_biriyani/api/data_models/order_save_data_model.dart';
import 'package:bay_of_biriyani/api/data_models/otp_verify_data_model.dart';
import 'package:bay_of_biriyani/api/data_models/promo_data_model.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:get/get_connect.dart';
import 'package:get/get_connect/http/src/http.dart';

class Api extends GetConnect {
  static Api instance = Api();

  var duration = Duration(seconds: 20);

  Future<HomeDataModel> getHomeData({Map<String, String> body}) async =>
      await post('${Constants.BASE_URL}/home', body)
          .then((response) => HomeDataModel.fromJson(response.body))
          .onError((error, stackTrace) => throw Exception("error -> $error"))
          .timeout(duration);

  Future<ItemDetailsDataModel> getItemDetailsData(
          {Map<String, String> body}) async =>
      await post("${Constants.BASE_URL}/item-details", body)
          .then((response) => ItemDetailsDataModel.fromJson(response.body))
          .onError((error, stackTrace) => throw Exception("error -> $error}"))
          .timeout(duration);

  Future<PromoDataModel> getPromoData({Map<String, String> body}) async =>
      await post("${Constants.BASE_URL}/promo", body)
          .then((response) => PromoDataModel.fromJson(response.body))
          .onError((error, stackTrace) => throw Exception("error -> $error}"))
          .timeout(duration);

  Future<OrderHistoryDataModel> getOrderHistoryData(
          {Map<String, String> body}) async =>
      await post("${Constants.BASE_URL}/orders", body)
          .then((response) => OrderHistoryDataModel.fromJson(response.body))
          .onError((error, stackTrace) => throw Exception("error -> $error}"))
          .timeout(duration);

  Future<OtpVerifyDataModel> getOtpVerifyData(
          {Map<String, String> body}) async =>
      await post("${Constants.BASE_URL}/otp-verify", body)
          .then((response) => OtpVerifyDataModel.fromJson(response.body))
          .onError((error, stackTrace) => throw Exception("error -> $error}"))
          .timeout(duration);

  Future<OrderSaveDataModel> getOrderSaveData(
          {Map<String, String> body}) async =>
      await post("${Constants.BASE_URL}/order-save", body, contentType: "application/json")
          .then((response) => OrderSaveDataModel.fromJson(response.body))
          .onError((error, stackTrace) => throw Exception("error -> $error}"))
          .timeout(duration);
}
// => OrderSaveDataModel.fromJson(response.body)