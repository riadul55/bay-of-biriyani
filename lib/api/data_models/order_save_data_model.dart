class OrderSaveDataModel {
  int statusCode;
  String message;
  String accessToken;

  OrderSaveDataModel(
      {this.statusCode, this.message, this.accessToken});

  OrderSaveDataModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    accessToken = json['access_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    data['message'] = this.message;
    data['access_token'] = this.accessToken;
    return data;
  }
}
