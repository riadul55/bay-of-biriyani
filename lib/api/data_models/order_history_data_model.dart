class OrderHistoryDataModel {
  int statusCode;
  int customStatusCode;
  String message;
  List<Data> data;

  OrderHistoryDataModel(
      {this.statusCode, this.customStatusCode, this.message, this.data});

  OrderHistoryDataModel.fromJson(Map<String, dynamic> json) {
    if (json.isNotEmpty) {
      statusCode = json['status_code'];
      customStatusCode = json['custom_status_code'];
      message = json['message'];
      if (json['data'] != null) {
        data = [];
        json['data'].forEach((v) {
          data.add(new Data.fromJson(v));
        });
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    data['custom_status_code'] = this.customStatusCode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String orderInvoice;
  int totalAmount;
  int deliveryCharge;
  int vat;
  Null promoCode;
  int discountAmount;
  int paidAmount;
  int customerId;
  String paymentType;
  int paymentStatus;
  int orderStatus;
  Null notes;
  int restaurantId;
  String areaId;
  int commissionRate;
  String createdAt;
  String updatedAt;
  List<Details> details;

  Data(
      {this.id,
      this.orderInvoice,
      this.totalAmount,
      this.deliveryCharge,
      this.vat,
      this.promoCode,
      this.discountAmount,
      this.paidAmount,
      this.customerId,
      this.paymentType,
      this.paymentStatus,
      this.orderStatus,
      this.notes,
      this.restaurantId,
      this.areaId,
      this.commissionRate,
      this.createdAt,
      this.updatedAt,
      this.details});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderInvoice = json['order_invoice'];
    totalAmount = json['total_amount'];
    deliveryCharge = json['delivery_charge'];
    vat = json['vat'];
    promoCode = json['promo_code'];
    discountAmount = json['discount_amount'];
    paidAmount = json['paid_amount'];
    customerId = json['customer_id'];
    paymentType = json['payment_type'];
    paymentStatus = json['payment_status'];
    orderStatus = json['order_status'];
    notes = json['notes'];
    restaurantId = json['restaurant_id'];
    areaId = json['area_id'];
    commissionRate = json['commission_rate'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['details'] != null) {
      details = [];
      json['details'].forEach((v) {
        details.add(new Details.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_invoice'] = this.orderInvoice;
    data['total_amount'] = this.totalAmount;
    data['delivery_charge'] = this.deliveryCharge;
    data['vat'] = this.vat;
    data['promo_code'] = this.promoCode;
    data['discount_amount'] = this.discountAmount;
    data['paid_amount'] = this.paidAmount;
    data['customer_id'] = this.customerId;
    data['payment_type'] = this.paymentType;
    data['payment_status'] = this.paymentStatus;
    data['order_status'] = this.orderStatus;
    data['notes'] = this.notes;
    data['restaurant_id'] = this.restaurantId;
    data['area_id'] = this.areaId;
    data['commission_rate'] = this.commissionRate;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.details != null) {
      data['details'] = this.details.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Details {
  String foodName;
  int id;
  int itemId;
  int sellingPrice;
  int quantity;
  String orderInvoice;
  Null size;
  String createdAt;
  String updatedAt;

  Details(
      {this.foodName,
      this.id,
      this.itemId,
      this.sellingPrice,
      this.quantity,
      this.orderInvoice,
      this.size,
      this.createdAt,
      this.updatedAt});

  Details.fromJson(Map<String, dynamic> json) {
    foodName = json['food_name'];
    id = json['id'];
    itemId = json['item_id'];
    sellingPrice = json['selling_price'];
    quantity = json['quantity'];
    orderInvoice = json['order_invoice'];
    size = json['size'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['food_name'] = this.foodName;
    data['id'] = this.id;
    data['item_id'] = this.itemId;
    data['selling_price'] = this.sellingPrice;
    data['quantity'] = this.quantity;
    data['order_invoice'] = this.orderInvoice;
    data['size'] = this.size;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
