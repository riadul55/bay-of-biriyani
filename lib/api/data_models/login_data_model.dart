class LoginDataModel {
  int statusCode;
  String message;
  Data data;

  LoginDataModel({this.statusCode, this.message, this.data});

  LoginDataModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String email;
  String password;
  String appSecretKey;

  Data({this.email, this.password, this.appSecretKey});

  Data.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    appSecretKey = json['app_secret_key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    data['app_secret_key'] = this.appSecretKey;
    return data;
  }
}
