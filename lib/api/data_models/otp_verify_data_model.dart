class OtpVerifyDataModel {
  int statusCode;
  int customStatusCode;
  String message;
  Data data;

  OtpVerifyDataModel(
      {this.statusCode, this.customStatusCode, this.message, this.data});

  OtpVerifyDataModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    customStatusCode = json['custom_status_code'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    data['custom_status_code'] = this.customStatusCode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String appSecretKey;
  String phoneNumber;
  String type;
  String otp;

  Data({this.appSecretKey, this.phoneNumber, this.type, this.otp});

  Data.fromJson(Map<String, dynamic> json) {
    appSecretKey = json['app_secret_key'];
    phoneNumber = json['phone_number'];
    type = json['type'];
    otp = json['otp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['app_secret_key'] = this.appSecretKey;
    data['phone_number'] = this.phoneNumber;
    data['type'] = this.type;
    data['otp'] = this.otp;
    return data;
  }
}
