class ItemDetailsDataModel {
  int statusCode;
  String message;
  String accessToken;
  FoodItems foodItems;

  ItemDetailsDataModel(
      {this.statusCode, this.message, this.accessToken, this.foodItems});

  ItemDetailsDataModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    message = json['message'];
    accessToken = json['access_token'];
    foodItems = json['food_items'] != null
        ? new FoodItems.fromJson(json['food_items'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    data['message'] = this.message;
    data['access_token'] = this.accessToken;
    if (this.foodItems != null) {
      data['food_items'] = this.foodItems.toJson();
    }
    return data;
  }
}

class FoodItems {
  int id;
  String foodName;
  Null foodDescription;
  String foodImage;
  int foodRegularPrice;
  int foodSellingPrice;
  int foodCategoryId;
  String createdAt;
  String updatedAt;
  String categoryName;
  int catId;
  int charge;

  FoodItems(
      {this.id,
        this.foodName,
        this.foodDescription,
        this.foodImage,
        this.foodRegularPrice,
        this.foodSellingPrice,
        this.foodCategoryId,
        this.createdAt,
        this.updatedAt,
        this.categoryName,
        this.catId,
        this.charge});

  FoodItems.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    foodName = json['food_name'];
    foodDescription = json['food_description'];
    foodImage = json['food_image'];
    foodRegularPrice = json['food_regular_price'];
    foodSellingPrice = json['food_selling_price'];
    foodCategoryId = json['food_category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    categoryName = json['category_name'];
    catId = json['cat_id'];
    charge = json['charge'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['food_name'] = this.foodName;
    data['food_description'] = this.foodDescription;
    data['food_image'] = this.foodImage;
    data['food_regular_price'] = this.foodRegularPrice;
    data['food_selling_price'] = this.foodSellingPrice;
    data['food_category_id'] = this.foodCategoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['category_name'] = this.categoryName;
    data['cat_id'] = this.catId;
    data['charge'] = this.charge;
    return data;
  }
}
