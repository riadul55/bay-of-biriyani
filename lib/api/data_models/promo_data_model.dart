class PromoDataModel {
  int statusCode;
  int customStatusCode;
  String message;
  Data data;
  String accessToken;
  int discount;

  PromoDataModel(
      {this.statusCode, this.customStatusCode, this.message, this.data,this.accessToken,this.discount});

  PromoDataModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['status_code'];
    customStatusCode = json['custom_status_code'];
    message = json['message'];
    if(statusCode == 400) data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    if(statusCode == 200){
      accessToken = json['access_token'];
      discount = json['discount'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_code'] = this.statusCode;
    data['custom_status_code'] = this.customStatusCode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String appSecretKey;
  String promoCode;
  String minSpend;

  Data({this.appSecretKey, this.promoCode, this.minSpend});

  Data.fromJson(Map<String, dynamic> json) {
    appSecretKey = json['app_secret_key'];
    promoCode = json['promo_code'];
    minSpend = json['min_spend'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['app_secret_key'] = this.appSecretKey;
    data['promo_code'] = this.promoCode;
    data['min_spend'] = this.minSpend;
    return data;
  }
}
