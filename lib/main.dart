import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'ui/carts/controller/cart_controller.dart';
import 'ui/home/controller/foods_controller.dart';
import 'ui/main/controller/main_controller.dart';
import 'ui/orders/controller/order_controller.dart';
import 'ui/splash/splash_screen.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final mainController = Get.put(MainController());
  final foodsController = Get.put(FoodsController());
  final cartController = Get.put(CartController());
  final orderController = Get.put(OrderController());

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      locale: Get.deviceLocale,
      title: 'app_title'.tr,
      defaultTransition: Transition.fade,
      opaqueRoute: Get.isOpaqueRouteDefault,
      popGesture: Get.isPopGestureEnable,
      transitionDuration: Get.defaultTransitionDuration,
      theme: Themes.lightTheme,
      home: SplashScreen(),
    );
  }
}

