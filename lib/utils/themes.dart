import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Themes {
  static final ThemeData lightTheme = ThemeData(
    primarySwatch: Colors.orange,
    primaryColor: Themes.primaryColor,
    accentColor: Themes.accentColor,
    backgroundColor: Themes.backgroundColor,
    accentIconTheme: IconThemeData(color: Themes.accentColor),
    fontFamily: Constants.FONT_FAMILY,
  );

  static final Color backgroundColor = Color.fromRGBO(248, 245, 242, 1);

  static final Color primaryColor = Color.fromRGBO(255, 239, 208, 1);
  static final Color secondaryColor = Color.fromRGBO(225, 225, 225, 1);
  static final Color accentColor = Color.fromRGBO(253, 175, 23, 1);
  static final Color badgeColor = Color.fromRGBO(237, 163, 69, 1);

  static final Color primaryText = Color.fromRGBO(62, 68, 98, 1);
  static final Color secondaryText = Color.fromRGBO(126, 126, 126, 1);

  // font style
  static TextStyle textStyle(
      {weight = FontWeight.w400,
      double size = 16.0,
      Color color = Colors.white}) {
    return TextStyle(
      color: color,
      fontFamily: Constants.FONT_FAMILY,
      fontSize: size,
      fontStyle: FontStyle.normal,
      fontWeight: weight,
    );
  }
}
