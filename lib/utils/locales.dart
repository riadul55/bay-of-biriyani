import 'package:get/get.dart';

class Locales extends Translations {

  @override
  Map<String, Map<String, String>> get keys => {
    'en_US': {
      'app_title': 'App Title',
    },
  };
}