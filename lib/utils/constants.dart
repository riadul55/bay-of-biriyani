class Constants {
  static final String BASE_URL = "https://food.eudocode.com/api/v1";
  static final String BASE_URL_IMAGE = "https://food.eudocode.com/";
  static final String APP_SECRETE_KEY = "123";

  static final String USER_LOCATION = "geoLocation";
  static final String USER_TOKEN = "token";

  // assets location
  static final String APP_LOGO = "assets/images/app_logo.png";
  static final String FOOD_DEMO_IMAGE = "assets/images/food_demo.png";
  static final String LOCATION_DEMO_IMAGE = "assets/images/location_demo.png";
  static final String ORDER_CONFIRMED_IMAGE = "assets/images/order_confirmed.png";
  static final String GEO_LOCATION_IMAGE = "assets/images/geo_location.png";
  static final String EMPTY_NOTIFY_DEMO_IMAGE = "assets/images/empty_notify.png";
  static final String LOCATION_MARKER_IMAGE = "assets/images/location_marker.png";

  //font family
  static final String FONT_FAMILY = "Poppins";

  //payment gateway credential
  static final String STORE_ID = "martv5fbf6e554e2ff";
  static final String STORE_PASSWORD = "martv5fbf6e554e2ff@ssl";
}
