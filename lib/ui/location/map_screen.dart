import 'dart:async';
import 'dart:math';

import 'package:bay_of_biriyani/ui/main/controller/main_controller.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

typedef OnSelectLocation(LatLng pos);

class MapScreen extends StatefulWidget {
  MapScreen({Key key}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  MainController mainController = Get.find();
  GetStorage _storage = GetStorage();

  Completer<GoogleMapController> _controller = Completer();

  static double latitude_current = 0.0;
  static double longitude_current = 0.0;

  static final CameraPosition _cameraPosition = CameraPosition(
      // bearing: 0.8334901395799,
      target: LatLng(latitude_current, longitude_current),
      // tilt: 80.440717697143555,
      zoom: 14.4746);

  bool _isMoving = false;
  bool _isLoading = false;
  LatLng _lastMapPosition;

  String _lastAddress = "";
  Timer callingTimer = null;

  @override
  void initState() {
    super.initState();
  }

  double getZoomLevel(double radius) {
    double zoomLevel = 11;
    if (radius > 0) {
      double radiusElevated = radius + radius / 2;
      double scale = radiusElevated / 500;
      zoomLevel = 16 - log(scale) / log(2);
    }
    zoomLevel = num.parse(zoomLevel.toStringAsFixed(2));
    return zoomLevel;
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        backgroundColor: Themes.backgroundColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Themes.backgroundColor,
          leading: Padding(
            padding: EdgeInsets.only(left: 16),
            child: CircleAvatar(
              backgroundColor: Themes.accentColor,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ),
          title: Text("Select Location"),
        ),
        body: Stack(
          children: [
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: LatLng(latitude_current, longitude_current),
                  // zoom: 14.4746,
                  zoom: 11.0,
                  // bearing: 1.0,
                ),
                myLocationEnabled: true,
                tiltGesturesEnabled: true,
                compassEnabled: true,
                scrollGesturesEnabled: true,
                zoomGesturesEnabled: true,
                minMaxZoomPreference: MinMaxZoomPreference(13, 17),
                onMapCreated: (GoogleMapController controller) async {
                  _controller.complete(controller);

                  var location = _storage.read(Constants.USER_LOCATION);
                  if (location != null) {
                    latitude_current = location['latitude'];
                    longitude_current = location['longitude'];
                  }

                  _lastMapPosition =
                      LatLng(latitude_current, longitude_current);
                  _goToTheDestination();

                  setState(() => _isMoving = true);
                },
                onCameraIdle: () async {
                  setState(() => {
                        _isMoving = false,
                        _isLoading = true,
                      });

                  await placemarkFromCoordinates(
                          _lastMapPosition.latitude, _lastMapPosition.longitude)
                      .then((List<Placemark> placemarks) {
                    print(placemarks);
                    var output = 'No results found.';
                    if (placemarks.isNotEmpty) {
                      output = placemarks[0].street;
                    }

                    setState(() {
                      _lastAddress = output;
                    });
                  });

                  setState(() => {
                        _isMoving = false,
                        _isLoading = false,
                      });
                },
                onCameraMove: (CameraPosition position) async {
                  _lastMapPosition = position.target;

                  setState(() => _isMoving = true);
                  // if (callingTimer != null) {
                  //   setState(() {
                  //     callingTimer.cancel();
                  //   });
                  // }
                  // setState(() {
                  //   callingTimer =
                  //       Timer(Duration(milliseconds: 2000), () async {
                  //     setState(() => {
                  //           _isMoving = false,
                  //           _isLoading = true,
                  //         });

                  //     await placemarkFromCoordinates(_lastMapPosition.latitude,
                  //             _lastMapPosition.longitude)
                  //         .then((List<Placemark> placemarks) {
                  //       print(placemarks);
                  //       var output = 'No results found.';
                  //       if (placemarks.isNotEmpty) {
                  //         output = placemarks[0].street;
                  //       }

                  //       setState(() {
                  //         _lastAddress = output;
                  //       });
                  //     });

                  //     setState(() => {
                  //           _isMoving = false,
                  //           _isLoading = false,
                  //         });
                  //   });
                  // });
                },
              ),
            ),
            Center(
              child: Container(
                width: 40,
                height: 40,
                child: Image.asset(
                  Constants.LOCATION_MARKER_IMAGE,
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          width: Get.width,
          height: 50,
          color: Themes.accentColor,
          child: getPrefWidget(),
        ),
      ),
    );
  }

  Widget getPrefWidget() {
    // if (_isMoving) {
    //   return ElevatedButton(
    //     child: Text(
    //       "Tap To Get Address",
    //       style: TextStyle(
    //         color: Colors.white,
    //       ),
    //     ),
    //     onPressed: () async {
    //       setState(() => {
    //             _isMoving = false,
    //             _isLoading = true,
    //           });

    //       await placemarkFromCoordinates(
    //               _lastMapPosition.latitude, _lastMapPosition.longitude)
    //           .then((List<Placemark> placemarks) {
    //         print(placemarks);
    //         var output = 'No results found.';
    //         if (placemarks.isNotEmpty) {
    //           output = placemarks[0].street;
    //         }

    //         setState(() {
    //           _lastAddress = output;
    //         });
    //       });

    //       setState(() => {
    //             _isMoving = false,
    //             _isLoading = false,
    //           });
    //     },
    //   );
    // } else {
    //   if (_isLoading) {
    //     return Center(
    //       child: CircularProgressIndicator(
    //         valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
    //       ),
    //     );
    //   } else {
    return Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.all(16),
            child: Text(
              _lastAddress,
              style: Themes.textStyle(),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        Container(
          height: 50,
          child: ElevatedButton(
            child: Icon(
              Icons.send,
              color: Colors.white,
            ),
            onPressed: () {
              _storage.write(Constants.USER_LOCATION, {
                "latitude": _lastMapPosition.latitude,
                "longitude": _lastMapPosition.longitude,
              });
              mainController.getAddressFromCoords();
              Get.back();
            },
          ),
        )
      ],
    );
    //   }
    // }
  }

  Future<void> _goToTheDestination() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_cameraPosition));
  }
}
