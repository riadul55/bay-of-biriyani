import 'package:bay_of_biriyani/ui/location/map_screen.dart';
import 'package:bay_of_biriyani/ui/main/main_screen.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/get_location.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GeoScreen extends StatefulWidget {
  @override
  _GeoScreenState createState() => _GeoScreenState();
}

class _GeoScreenState extends State<GeoScreen> {
  GetStorage boxStorage = GetStorage();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: Get.width,
                    height: 300,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(Constants.GEO_LOCATION_IMAGE),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  ),
                  Container(
                    width: Get.width,
                    child: _isLoading
                        ? Center(
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Themes.accentColor),
                            ),
                          )
                        : SubmitBtnWidgets(
                            text: "Use Current Location",
                            bgColor: Themes.accentColor,
                            borderColor: Themes.accentColor,
                            radius: 4.0,
                            padding: EdgeInsets.all(10),
                            onSubmit: () async {
                              // Get.off(() => MainScreen());
                              setState(() => _isLoading = true);
                              await GetLocation.determinePosition()
                                  .then((Position value) => {
                                        boxStorage
                                            .write(Constants.USER_LOCATION, {
                                          "latitude": value.latitude,
                                          "longitude": value.longitude,
                                        }),
                                        Get.off(() => MainScreen()),
                                      })
                                  .catchError((onError) {
                                print(onError);
                                Get.snackbar(
                                  "Message",
                                  onError.toString(),
                                  backgroundColor: Themes.accentColor,
                                  colorText: Colors.white,
                                ); // snackbar
                              });
                              setState(() => _isLoading = false);
                            },
                          ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    width: Get.width,
                    child: TextButton(
                      child: Text(
                        "Select Another Location",
                        style: Themes.textStyle(
                          size: 20,
                          color: Themes.accentColor,
                        ),
                      ),
                      onPressed: () {
                        if (_isLoading) {
                          Get.snackbar(
                            "Message",
                            "Running GEO Location",
                            backgroundColor: Themes.accentColor,
                            colorText: Colors.white,
                          );
                        } else {
                          Get.to(() => MapScreen());
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
