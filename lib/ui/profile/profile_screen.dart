import 'package:bay_of_biriyani/ui/auth/login_screen.dart';
import 'package:bay_of_biriyani/ui/location/map_screen.dart';
import 'package:bay_of_biriyani/ui/main/widgets/custom_app_bar.dart';
import 'package:bay_of_biriyani/ui/notification/notification_screen.dart';
import 'package:bay_of_biriyani/ui/orders/orders_screen.dart';
import 'package:bay_of_biriyani/ui/profile/profile_update_screen.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: CustomAppBar(),
        ),
        body: Container(
          padding: EdgeInsets.only(top: 20, left: 16, right: 16),
          child: ListView(
            children: [
              ListTile(
                leading: Icon(
                  Icons.account_circle,
                  size: 60,
                  color: Themes.accentColor,
                ),
                title: Text(
                  "Leslie Alexander",
                  style: Themes.textStyle(
                    size: 24,
                    color: Themes.primaryText,
                    weight: FontWeight.w500,
                  ),
                ),
                subtitle: Text("01XXXXXXXXXX"),
                onTap: () {
                  Get.to(() => ProfileUpdateScreen());
                },
              ),
              SizedBox(
                height: 30,
              ),
              ListTile(
                leading: Icon(
                  Icons.notifications_none,
                  size: 30,
                ),
                title: Text(
                  "Notification",
                  style: Themes.textStyle(size: 20, color: Themes.primaryText,),
                ),
                onTap: () {
                  Get.to(() => NotificationScreen());
                },
              ),
              SizedBox(
                height: 10,
              ),
              ListTile(
                leading: Icon(
                  Icons.shopping_bag_outlined,
                  size: 30,
                ),
                title: Text(
                  "Orders",
                  style: Themes.textStyle(size: 20, color: Themes.primaryText,),
                ),
                onTap: () {
                  Get.to(() => OrdersScreen(frommHome: false,));
                },
              ),
              SizedBox(
                height: 10,
              ),
              ListTile(
                leading: Icon(
                  Icons.location_city_outlined,
                  size: 30,
                ),
                title: Text(
                  "My Location",
                  style: Themes.textStyle(size: 20, color: Themes.primaryText,),
                ),
                onTap: () {
                  Get.to(() => MapScreen());
                },
              ),
              SizedBox(
                height: 10,
              ),
              ListTile(
                leading: Icon(
                  Icons.login_outlined,
                  size: 30,
                ),
                title: Text(
                  "Login",
                  style: Themes.textStyle(size: 20, color: Themes.primaryText,),
                ),
                onTap: () {
                  Get.to(() => LoginScreen());
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
