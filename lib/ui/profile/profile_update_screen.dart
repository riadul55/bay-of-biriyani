import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/ui/widgets/input_widgets.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileUpdateScreen extends StatefulWidget {
  @override
  _ProfileUpdateScreenState createState() => _ProfileUpdateScreenState();
}

class _ProfileUpdateScreenState extends State<ProfileUpdateScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = Get.size;
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Themes.backgroundColor,
          leading: Padding(
            padding: EdgeInsets.only(left: 16),
            child: CircleAvatar(
              backgroundColor: Themes.accentColor,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ),
          title: Text("Edit Profile"),
        ),
        body: SingleChildScrollView(
          child: Container(
            height: size.height,
            width: size.width,
            padding: EdgeInsets.only(
              top: size.height * 0.12,
              left: size.width * 0.15,
              right: size.width * 0.15,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InputWidget(
                  labelText: "Full Name",
                  onChanged: (value) {},
                ),
                SizedBox(height: size.height * 0.03),
                InputWidget(
                  labelText: "Phone",
                  onChanged: (value) {},
                ),
                SizedBox(height: size.height * 0.03),
                InputWidget(
                  labelText: "Email",
                  onChanged: (value) {},
                ),
                SizedBox(height: size.height * 0.07),
                Container(
                  width: Get.width,
                  child: SubmitBtnWidgets(
                    text: "Save",
                    bgColor: Themes.accentColor,
                    borderColor: Themes.accentColor,
                    radius: 4.0,
                    padding: EdgeInsets.all(10),
                    onSubmit: () {
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
