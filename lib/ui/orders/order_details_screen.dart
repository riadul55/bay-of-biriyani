import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderDetailsScreen extends StatefulWidget {
  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<OrderDetailsScreen> {
  var title_text_style = Themes.textStyle(
    color: Themes.primaryText,
  );

  var value_text_style =
      Themes.textStyle(color: Themes.primaryText, weight: FontWeight.w600);

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Themes.backgroundColor,
          leading: Padding(
            padding: EdgeInsets.only(left: 16),
            child: CircleAvatar(
              backgroundColor: Themes.accentColor,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ),
          title: Text("Order Details"),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Your order number",
                          style: title_text_style,
                        ),
                        Text(
                          "#45862",
                          style: value_text_style,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Delivery Address",
                          style: title_text_style,
                        ),
                        Container(
                          constraints: BoxConstraints(
                            maxWidth: 200,
                          ),
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Text(
                              "Tolarbagh, Mirpur",
                              style: value_text_style,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    OrderedItemList(),
                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Subtotal",
                          style: title_text_style,
                        ),
                        Text(
                          "Tk 400",
                          style: value_text_style,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Delivery fee",
                          style: title_text_style,
                        ),
                        Text(
                          "Tk 50",
                          style: value_text_style,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Incl. Tax",
                          style: title_text_style,
                        ),
                        Text(
                          "Tk 10",
                          style: value_text_style,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Total (Incl. VAT)",
                          style: Themes.textStyle(
                            size: 20,
                            color: Themes.primaryText,
                            weight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          "Tk 460",
                          style: Themes.textStyle(
                            size: 20,
                            color: Themes.primaryText,
                            weight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 200,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 20,
              left: 20,
              right: 20,
              child: Container(
                width: Get.width,
                child: SubmitBtnWidgets(
                  text: "Review Payment And Address",
                  bgColor: Themes.accentColor,
                  borderColor: Themes.accentColor,
                  radius: 4.0,
                  padding: EdgeInsets.all(10),
                  onSubmit: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  OrderedItemList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(
        10,
        (index) => Container(
          padding: EdgeInsets.only(
            bottom: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "2X",
                style: value_text_style,
              ),
              Text(
                "2 X Kacchi",
                style: title_text_style,
              ),
              Text(
                "Tk 400",
                style: title_text_style,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
