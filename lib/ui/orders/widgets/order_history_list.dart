import 'package:bay_of_biriyani/api/data_models/order_history_data_model.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../order_details_screen.dart';

class OrderHistoryList extends StatefulWidget {
  OrderHistoryList({Key key, @required this.data}) : super(key: key);

  final OrderHistoryDataModel data;

  @override
  _OrderHistoryListState createState() => _OrderHistoryListState();
}

class _OrderHistoryListState extends State<OrderHistoryList> {

  List<Data> data = [];

  @override
  void initState() {
    super.initState();
    data = widget.data.data;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: EdgeInsets.all(16),
      child: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            child: Container(
              padding: EdgeInsets.only(
                top: 32,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          "#${data[index].orderInvoice}",
                          style: Themes.textStyle(
                              color: Themes.primaryText,
                              weight: FontWeight.w500),
                        ),
                      ),
                      Text(
                        "Tk ${data[index].totalAmount}",
                        style: Themes.textStyle(
                            color: Themes.primaryText,
                            weight: FontWeight.w500),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: TextButton(
                      child: Text(
                        "Reorder",
                        style: Themes.textStyle(
                            color: Themes.accentColor,
                            weight: FontWeight.w500),
                      ),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
            onTap: () {
              Get.to(() => OrderDetailsScreen());
            },
          );
        },
      ),
    );
  }
}
