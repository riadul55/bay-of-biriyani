import 'package:bay_of_biriyani/ui/main/widgets/custom_app_bar.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller/order_controller.dart';
import 'order_details_screen.dart';
import 'widgets/order_history_list.dart';
import 'widgets/order_history_shimmer.dart';

class OrdersScreen extends StatefulWidget {
  final bool frommHome;
  OrdersScreen({Key key, this.frommHome}) : assert(key == null);
  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  OrderController orderController = Get.find();

  @override
  void initState() {
    super.initState();
    orderController.onFetchOrderHistory(phone: "01717849968");
  }

  @override
  void dispose() {
    // try {
    //   orderController.dispose();
    // } catch(e) {
    //   print(e);
    // }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        appBar: widget.frommHome ? PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: CustomAppBar(),
        ) : AppBar(
          elevation: 0,
          backgroundColor: Themes.backgroundColor,
          leading: Padding(
            padding: EdgeInsets.only(left: 16),
            child: CircleAvatar(
              backgroundColor: Themes.accentColor,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ),
          title: Text("Order History"),
        ),
        body: Obx(
          () => orderController.dataAvailable
              ? OrderHistoryList(
                  data: orderController.responseData,
                )
              : OrderHistoryShimmer(),
        ),
      ),
    );
  }
}
