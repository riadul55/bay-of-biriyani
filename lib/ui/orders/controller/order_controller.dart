import 'package:bay_of_biriyani/api/api.dart';
import 'package:bay_of_biriyani/api/body/api_body.dart';
import 'package:bay_of_biriyani/api/data_models/order_history_data_model.dart';
import 'package:get/get.dart';

class OrderController extends GetxController {
  static OrderController instance = Get.find();

  var _dataAvailable = false.obs;
  var _data;

  bool get dataAvailable => _dataAvailable.value;

  OrderHistoryDataModel get responseData => _data;

  void onFetchOrderHistory({phone}) {
    Api.instance
        .getOrderHistoryData(body: ApiBody.getOrderHistoryBody(phone: phone))
        .then((value) {
      if (value != null) {
        _data = value;
      }
    }).onError((error, stackTrace) {
      print(error.toString());
    }).whenComplete(() => _dataAvailable.value = _data != null);
  }

  void clearOrderHistory() {
    _dataAvailable.value = false;
  }

  @override
  void dispose() {
    super.dispose();
  }
}