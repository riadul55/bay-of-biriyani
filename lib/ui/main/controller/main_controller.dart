import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/state_manager.dart';
import 'package:get_storage/get_storage.dart';

class MainController extends GetxController {

  GetStorage _storage = GetStorage();

  var currentScreen = 0.obs;
  var locationAddress = "".obs;

  @override
  void onInit() {
    super.onInit();

    getScreenPosition();
  }

  int getScreenPosition() {
    return currentScreen.value;
  }

  setScreenPosition(int pos) {
    currentScreen.value = pos;
  }

  getAddressFromCoords() async {
    var location = _storage.read(Constants.USER_LOCATION);
    await placemarkFromCoordinates(location['latitude'], location['longitude'])
        .then((List<Placemark> placemarks) {
      print(placemarks);
      var output = 'No results found.';
      if (placemarks.isNotEmpty) {
        output = placemarks[0].name;
      }
      locationAddress(output);
    });
  }
}