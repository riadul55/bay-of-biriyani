import 'package:bay_of_biriyani/ui/carts/cart_screen.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/home/controller/foods_controller.dart';
import 'package:bay_of_biriyani/ui/home/home_screen.dart';
import 'package:bay_of_biriyani/ui/main/controller/main_controller.dart';
import 'package:bay_of_biriyani/ui/main/widgets/custom_app_bar.dart';
import 'package:bay_of_biriyani/ui/orders/orders_screen.dart';
import 'package:bay_of_biriyani/ui/profile/profile_screen.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'widgets/bottom_nav.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<Widget> screens = [
    HomeScreen(),
    OrdersScreen(
      frommHome: true,
    ),
    ProfileScreen(),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: GetX<MainController>(builder: (controller) {
        return Scaffold(
          appBar: null,
          backgroundColor: Themes.backgroundColor,
          body: screens[controller.getScreenPosition()],
          bottomNavigationBar: BottomNav(
            position: controller.getScreenPosition(),
            onChangeNav: (position) {
              controller.setScreenPosition(position);
            },
          ),
        );
      }),
      onWillPop: () {
        return null;
      },
    );
  }
}
