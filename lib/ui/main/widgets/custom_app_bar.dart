import 'package:badges/badges.dart';
import 'package:bay_of_biriyani/ui/carts/cart_screen.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/main/controller/main_controller.dart';
import 'package:bay_of_biriyani/ui/notification/notification_screen.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomAppBar extends StatelessWidget {
  MainController mainController = Get.find();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Themes.backgroundColor,
      leading: Padding(
        padding: EdgeInsets.only(left: 16),
        child: CircleAvatar(
          backgroundColor: Themes.accentColor,
          child: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              mainController.setScreenPosition(0);
            },
          ),
        ),
      ),
      actions: [
        CircleAvatar(
          backgroundColor: Themes.accentColor,
          child: IconButton(
            icon: Badge(
              showBadge: true,
              badgeColor: Themes.badgeColor,
              child: Icon(
                Icons.notifications_none_outlined,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              Get.to(() => NotificationScreen());
            },
          ),
        ),
        SizedBox(
          width: 10,
        ),
        CircleAvatar(
          backgroundColor: Themes.accentColor,
          child: IconButton(
            icon: Badge(
              badgeContent: GetX<CartController>(
                builder: (controller) {
                  if (controller.cartList.length > 0) {
                    return Text(
                      controller.cartList.length.toString(),
                      style: TextStyle(color: Colors.white),
                    );
                  } else {
                    return Text("");
                  }
                },
              ),
              showBadge: true,
              badgeColor: Themes.badgeColor,
              child: Icon(
                Icons.shopping_bag_outlined,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              Get.to(() => CartScreen());
              // mainController.setScreenPosition(1);
            },
          ),
        ),
        SizedBox(
          width: 16,
        ),
      ],
    );
  }
}

class CustomAppBar2 extends StatelessWidget {
  MainController mainController = Get.find();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Themes.backgroundColor,
      leading: Padding(
        padding: EdgeInsets.only(left: 16),
        child: CircleAvatar(
          backgroundColor: Themes.accentColor,
          child: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              Get.back();
              // mainController.setScreenPosition(0);
            },
          ),
        ),
      ),
      actions: [
        CircleAvatar(
          backgroundColor: Themes.accentColor,
          child: IconButton(
            icon: Badge(
              showBadge: true,
              badgeColor: Themes.badgeColor,
              child: Icon(
                Icons.notifications_none_outlined,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              Get.to(() => NotificationScreen());
            },
          ),
        ),
        SizedBox(
          width: 10,
        ),
        CircleAvatar(
          backgroundColor: Themes.accentColor,
          child: IconButton(
            icon: Badge(
              badgeContent: GetX<CartController>(
                builder: (controller) {
                  if (controller.cartList.length > 0) {
                    return Text(
                      controller.cartList.length.toString(),
                      style: TextStyle(color: Colors.white),
                    );
                  } else {
                    return Text("");
                  }
                },
              ),
              showBadge: true,
              badgeColor: Themes.badgeColor,
              child: Icon(
                Icons.shopping_bag_outlined,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              mainController.setScreenPosition(1);
            },
          ),
        ),
        SizedBox(
          width: 16,
        ),
      ],
    );
  }
}
