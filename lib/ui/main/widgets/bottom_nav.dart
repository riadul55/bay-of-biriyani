import 'package:badges/badges.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';

typedef void OnChangeNav(int position);

class BottomNav extends StatelessWidget {
  BottomNav({this.onChangeNav, this.position});
  final OnChangeNav onChangeNav;
  final int position;

  List<IconData> navList = [
    Icons.home_outlined,
    Icons.history_sharp,
    Icons.person_outline_outlined,
  ];

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: navList.map<BottomNavigationBarItem>((item) {
        int index = navList.indexOf(item);
        return BottomNavigationBarItem(
          label: "",
          icon: index == 1
              ?
              //    Badge(
              // badgeColor: Themes.badgeColor,
              // position: BadgePosition.bottomEnd(bottom: 0, end: 0),
              // child:
              Stack(
                  children: [
                    Icon(
                      item,
                      size: 30,
                      color: position == index
                          ? Themes.accentColor
                          : Themes.primaryText,
                    ),
                    // Positioned(
                    //   left: 0,
                    //   right: 0,
                    //   bottom: 0,
                    //   top: 0,
                    //   child: Icon(
                    //     Icons.shopping_cart_sharp,
                    //     size: 10,
                    //     color: position == index
                    //         ? Themes.accentColor
                    //         : Themes.primaryText,
                    //   ),
                    // ),
                  ],
                )
              // )
              : Icon(
                  item,
                  size: 30,
                  color: position == index
                      ? Themes.accentColor
                      : Themes.primaryText,
                ),
        );
      }).toList(),
      onTap: (pos) {
        onChangeNav(pos);
      },
    );
  }
}


// class BottomNav extends StatefulWidget {
//   BottomNav({Key key, this.onChangeNav, this.position}) : super(key: key);
//   final OnChangeNav onChangeNav;
//   final int position;
//
//   @override
//   _BottomNavState createState() => _BottomNavState(position);
// }
//
// class _BottomNavState extends State<BottomNav> {
//   _BottomNavState(this.currentIndex);
//   int currentIndex;
//
//   List<IconData> navList = [
//     Icons.home_outlined,
//     Icons.shopping_bag_outlined,
//     Icons.person_outline_outlined,
//   ];
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return BottomNavigationBar(
//       items: navList.map<BottomNavigationBarItem>((item) {
//         int index = navList.indexOf(item);
//         return BottomNavigationBarItem(
//           label: "",
//           icon: index == 1
//               ? Badge(
//             badgeColor: Themes.badgeColor,
//             position: BadgePosition.bottomEnd(bottom: 0, end: 0),
//             child: Icon(
//               item,
//               size: 30,
//               color: currentIndex == index
//                   ? Themes.accentColor
//                   : Themes.primaryText,
//             ),
//           )
//               : Icon(
//             item,
//             size: 30,
//             color: currentIndex == index
//                 ? Themes.accentColor
//                 : Themes.primaryText,
//           ),
//         );
//       }).toList(),
//       onTap: (pos) {
//         widget.onChangeNav(pos);
//         setState(() {
//           currentIndex = pos;
//         });
//       },
//     );
//   }
// }

