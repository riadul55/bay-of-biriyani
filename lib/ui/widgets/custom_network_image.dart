import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CustomNetworkImage extends StatelessWidget {
  CustomNetworkImage({
    @required this.image,
    this.width,
    this.height,
    this.errorIcon,
    this.errorIconSize,
  });

  String image;
  double width;
  double height;
  IconData errorIcon = Icons.error_outline;
  double errorIconSize = 40.0;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: "${Constants.BASE_URL_IMAGE}$image",
      imageBuilder: (context, imageProvider) => Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.fill,
            colorFilter: ColorFilter.mode(
              Colors.white,
              BlendMode.colorBurn,
            ),
          ),
        ),
      ),
      placeholder: (context, url) => Container(
        width: width,
        height: height,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
      errorWidget: (context, url, error) => Container(
        width: width,
        height: height,
        child: Center(
          child: Icon(
            errorIcon,
            color: Themes.accentColor,
            size: errorIconSize,
          ),
        ),
      ),
    );
  }
}
