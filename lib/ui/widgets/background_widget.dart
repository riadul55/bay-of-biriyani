import 'package:flutter/material.dart';

class BackgroundWidget extends StatelessWidget {
  BackgroundWidget({
    this.statusBar = Colors.orange,
    this.child,
  });

  final Color statusBar;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: statusBar,
      body: SafeArea(
        child: child,
      ),
    );
  }
}
