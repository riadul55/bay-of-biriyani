import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';

typedef void OnSubmitButton();

class SubmitBtnWidgets extends StatelessWidget {
  SubmitBtnWidgets({
    Key key,
    this.text = 'Submit',
    this.radius = 0.0,
    this.textColor = Colors.white,
    this.bgColor = Colors.orangeAccent,
    this.borderColor = Colors.orangeAccent,
    this.padding = const EdgeInsets.all(0.0),
    this.onSubmit,
  }) : super(key: key);

  final String text;
  final double radius;
  final Color textColor;
  final Color bgColor;
  final Color borderColor;
  final EdgeInsets padding;
  final OnSubmitButton onSubmit;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(bgColor),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
            side: BorderSide(color: borderColor),
          ),
        ),
        padding: MaterialStateProperty.all(padding),
      ),
      child: Text(
        text,
        style: Themes.textStyle(size: 20, color: textColor),
      ),
      onPressed: () => onSubmit(),
    );
  }
}
