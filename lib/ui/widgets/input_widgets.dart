import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';

class InputWidget extends StatelessWidget {
  InputWidget({
    Key key,
    this.labelText,
    this.hintText,
    this.inputType = TextInputType.text,
    this.obscureText = false,
    this.onChanged,
    this.validate = true,
    this.errorText = '',
  }) : super(key: key);

  final String labelText;
  final String hintText;
  final TextInputType inputType;
  final bool obscureText;
  final bool validate;
  final String errorText;
  final ValueChanged<String> onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        labelText != null
            ? Text(
                labelText,
                style: Themes.textStyle(color: Themes.primaryText,),
              )
            : Container(),
        SizedBox(
          height: 10,
        ),
        TextField(
          obscureText: obscureText,
          onChanged: (value) {
            onChanged(value);
          },
          cursorColor: Themes.primaryText,
          keyboardType: inputType,
          decoration: InputDecoration(
            filled: true,
            fillColor: Themes.primaryColor,
            hintText: hintText,
            hintStyle: TextStyle(color: Themes.secondaryText),
            border: OutlineInputBorder(
              borderSide: BorderSide.none,
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide.none,
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red,
                width: 1,
              ),
            ),
            errorText: !validate ? errorText : null,
          ),
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ],
    );
  }
}
