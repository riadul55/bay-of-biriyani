import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EmptyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Container(
            width: Get.width,
            child: Image.asset(Constants.EMPTY_NOTIFY_DEMO_IMAGE),
          ),
          Text(
            "Nothing Here!",
            style: Themes.textStyle(
              color: Themes.primaryText,
              weight: FontWeight.w300,
            ),
          ),
        ],
      ),
    );
  }
}
