import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Themes.backgroundColor,
          leading: Padding(
            padding: EdgeInsets.only(left: 16),
            child: CircleAvatar(
              backgroundColor: Themes.accentColor,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ),
          title: Text("Notifications"),
        ),
        body: ListView.builder(
          itemCount: 3,
          itemBuilder: (context, index) {
            return Container(
              width: Get.width,
              padding: EdgeInsets.all(20),
              child: Text(
                "All logos on Logoipsum collection are made to be abstract and flexible enough to fit with any graphic, ",
                style: Themes.textStyle(
                  color: Themes.primaryText,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
