import 'package:bay_of_biriyani/api/api.dart';
import 'package:get/get.dart';

class NotifyController extends GetxController {
  var _dataAvailable = false.obs;
  var _data;

  bool get dataAvailable => _dataAvailable.value;

  get responseData => _data;

  // notification fetching will be here
  void onFetchNotification() {
    // Api.instance
    //     .getNotifications(body: )
    //     .then((value) {
    //   if (value != null) {
    //     _data = value;
    //   }
    // }).onError((error, stackTrace) {
    //   print(error.toString());
    // }).whenComplete(() => _dataAvailable.value = _data != null);
  }
}