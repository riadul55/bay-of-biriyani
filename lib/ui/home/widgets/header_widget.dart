import 'package:badges/badges.dart';
import 'package:bay_of_biriyani/ui/carts/cart_screen.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/location/map_screen.dart';
import 'package:bay_of_biriyani/ui/main/controller/main_controller.dart';
import 'package:bay_of_biriyani/ui/notification/notification_screen.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HeaderWidget extends StatefulWidget {
  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  MainController mainController = Get.find();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Row(
        children: [
          Expanded(
            child: TextButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.zero),
              ),
              child: Container(
                padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                child: Row(
                  children: [
                    Expanded(
                      child: Obx(() {
                        return Text(
                          mainController.locationAddress.value,
                          style: Themes.textStyle(
                            size: 20,
                            color: Themes.secondaryText,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        );
                      }),
                    ),
                    Icon(Icons.keyboard_arrow_down),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Themes.primaryColor,
                  borderRadius: BorderRadius.all(
                    Radius.circular(34),
                  ),
                ),
              ),
              onPressed: () {
                Get.to(() => MapScreen());
              },
            ),
          ),
          SizedBox(
            width: 10,
          ),
          CircleAvatar(
            backgroundColor: Themes.accentColor,
            child: IconButton(
              icon: Badge(
                showBadge: true,
                badgeColor: Themes.badgeColor,
                child: Icon(
                  Icons.notifications_none_outlined,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                Get.to(() => NotificationScreen());
              },
            ),
          ),
          SizedBox(
            width: 10,
          ),
          CircleAvatar(
            backgroundColor: Themes.accentColor,
            child: IconButton(
              icon: Badge(
                badgeContent: GetX<CartController>(
                  builder: (controller) {
                    if (controller.cartList.length > 0) {
                      return Text(
                        controller.cartList.length.toString(),
                        style: TextStyle(color: Colors.white),
                      );
                    } else {
                      return Text("");
                    }
                  },
                ),
                showBadge: true,
                badgeColor: Themes.badgeColor,
                child: Icon(
                  Icons.shopping_bag_outlined,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                Get.to(() => CartScreen());
                // mainController.setScreenPosition(1);
              },
            ),
          ),
        ],
      ),
    );
  }
}
