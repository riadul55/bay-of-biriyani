import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';

class GreetingsWidget extends StatefulWidget {
  @override
  _GreetingsWidgetState createState() => _GreetingsWidgetState();
}

class _GreetingsWidgetState extends State<GreetingsWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20, left: 16,),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              "Hello",
              style: Themes.textStyle(
                size: 38,
                color: Themes.primaryText,
                weight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            child: Text(
              "What do you want to eat?",
              style: Themes.textStyle(
                size: 20,
                color: Themes.secondaryText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
