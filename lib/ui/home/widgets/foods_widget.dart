import 'package:bay_of_biriyani/api/data_models/home_data_model.dart';
import 'package:bay_of_biriyani/local_db/AppDb.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/widgets/custom_network_image.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class FoodsWidget extends StatefulWidget {
  FoodsWidget({Key key, @required this.foods, @required this.catId})
      : super(key: key);

  final List<FoodItems> foods;
  final int catId;

  @override
  _FoodsWidgetState createState() => _FoodsWidgetState();
}

class _FoodsWidgetState extends State<FoodsWidget> {

  final List<FoodItems> foods = [];

  @override
  void initState() {
    super.initState();
    widget.foods.forEach((food) {
      if (widget.catId == food.catId) {
        foods.add(food);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 20,
        left: 16,
        right: 16,
      ),
      child: StaggeredGridView.countBuilder(
        crossAxisCount: 2,
        itemCount: foods.length,
        crossAxisSpacing: 10,
        mainAxisSpacing: 16,
        staggeredTileBuilder: (index) => StaggeredTile.fit(1),
        itemBuilder: (context, index) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: Colors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomNetworkImage(
                  image: foods[index].foodImage,
                  width: Get.width,
                  height: 180,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 10,
                    top: 10,
                    right: 10,
                    bottom: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          foods[index].foodName,
                          style: Themes.textStyle(
                            color: Themes.primaryText,
                            weight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  (foods[index].foodRegularPrice != foods[index].foodSellingPrice) ? TextSpan(
                                    text: "${foods[index].foodRegularPrice} bdt\n".toUpperCase(),
                                    style: Themes.textStyle(
                                      color: Themes.primaryText,
                                      weight: FontWeight.w400,
                                    ).copyWith(
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ) : TextSpan(),
                                  TextSpan(
                                    text: "${foods[index].foodSellingPrice} bdt".toUpperCase(),
                                    style: Themes.textStyle(
                                      color: Themes.accentColor,
                                      weight: FontWeight.w600,
                                    ),
                                  ),
                                ]
                              ),
                            ),
                          ),
                          GetX<CartController>(
                              builder: (controller) {
                                return controller.isCartAdded(foods[index]) ? IconButton(
                                  icon: Icon(
                                    Icons.remove_circle,
                                    color: Themes.accentColor,
                                    size: 30,
                                  ),
                                  onPressed: () {
                                    FoodItems food = foods[index];
                                    controller.removeCart(food.id);
                                  },
                                ) : IconButton(
                                  icon: Icon(
                                    Icons.add_circle,
                                    color: Themes.accentColor,
                                    size: 30,
                                  ),
                                  onPressed: () {
                                    FoodItems food = foods[index];
                                    controller.addToCart(FoodsTable(
                                        foodId: food.id,
                                        name: food.foodName,
                                        image: food.foodImage,
                                        price:
                                        food.foodSellingPrice.toString(),
                                        charge: food.charge.toString(),
                                        quantity: 1));
                                  },
                                );
                              }),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  // bool isCartAdded(RxList<dynamic> cartList, FoodItems food){
  //   return cartList.any((element) => element.foodId == food.id);
  // }
}
