import 'package:bay_of_biriyani/api/data_models/home_data_model.dart';
import 'package:bay_of_biriyani/ui/home/widgets/foods_widget.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'greetings_widget.dart';
import 'header_widget.dart';

class CategoriesWidget extends StatefulWidget {
  CategoriesWidget({Key key, @required this.data}) : super(key: key);

  final HomeDataModel data;

  @override
  _CategoriesWidgetState createState() => _CategoriesWidgetState();
}

class _CategoriesWidgetState extends State<CategoriesWidget>
    with TickerProviderStateMixin {
  TabController _tabController;


  List<FoodCategories> categories;
  List<FoodItems> foodItems;

  @override
  void initState() {
    super.initState();
    if (widget.data.foodCategories != null) {
      categories = widget.data.foodCategories;

      if (widget.data.foodItems != null) {
        foodItems = widget.data.foodItems;
      }

      initData();
    }

  }

  void initData() {
    _tabController = null;
    _tabController =
        TabController(length: categories.length, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {}
    });
  }

  @override
  void didUpdateWidget(covariant CategoriesWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    initData();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: categories.length,
      child: NestedScrollView(
        headerSliverBuilder: (context, value) {
          return [
            SliverAppBar(
              expandedHeight: 280,
              backgroundColor: Themes.backgroundColor,
              floating: true,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    HeaderWidget(),
                    SizedBox(
                      height: 20,
                    ),
                    GreetingsWidget(),
                  ],
                ),
              ),
              bottom: TabBar(
                controller: _tabController,
                labelColor: Colors.white,
                unselectedLabelColor: Themes.primaryText,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Themes.accentColor,
                ),
                isScrollable: true,
                tabs: categories.map((category) {
                  return ConstrainedBox(
                    constraints: new BoxConstraints(
                      minWidth: 100.0,
                    ),
                    child: Container(
                      padding: EdgeInsets.only(left: 16, right: 16,),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Tab(
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            category.categoryName,
                            style: TextStyle(
                              fontFamily: Constants.FONT_FAMILY,
                              fontSize: 16,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ),
            )
          ];
        },
        body: TabBarView(
          controller: _tabController,
          physics: AlwaysScrollableScrollPhysics(),
          children: categories.map((category) {
            return FoodsWidget(foods: foodItems, catId: category.id,);
          }).toList(),
        ),
      ),
    );
  }
}
