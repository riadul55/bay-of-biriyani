import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

import 'greetings_widget.dart';
import 'header_widget.dart';

class HomeShimmer extends StatefulWidget {
  @override
  _HomeShimmerState createState() => _HomeShimmerState();
}

class _HomeShimmerState extends State<HomeShimmer>
    with TickerProviderStateMixin {

  var categories = [0, 1, 2, 3];
  TabController _tabController;

  @override
  void initState() {
    initData();
    super.initState();
  }

  void initData() {
    _tabController = null;
    _tabController =
        TabController(length: categories.length, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {}
    });
  }

  @override
  void didUpdateWidget(covariant HomeShimmer oldWidget) {
    super.didUpdateWidget(oldWidget);
    initData();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: categories.length,
      child: NestedScrollView(
        headerSliverBuilder: (context, value) {
          return [
            SliverAppBar(
              expandedHeight: 280,
              backgroundColor: Themes.backgroundColor,
              floating: true,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    HeaderWidget(),
                    SizedBox(
                      height: 20,
                    ),
                    GreetingsWidget(),
                  ],
                ),
              ),
              bottom: TabBar(
                controller: _tabController,
                labelColor: Colors.white,
                unselectedLabelColor: Themes.primaryText,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.white,
                ),
                isScrollable: true,
                tabs: categories.map((category) {
                  return Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[200],
                    enabled: true,
                    child: ConstrainedBox(
                      constraints: new BoxConstraints(
                        minWidth: 100.0,
                      ),
                      child: Container(
                        padding: EdgeInsets.only(left: 16, right: 16,),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.grey,
                        ),
                        child: Tab(
                          child: Align(
                            alignment: Alignment.center,
                            child: Container(),
                          ),
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ),
            )
          ];
        },
        body: TabBarView(
          controller: _tabController,
          physics: AlwaysScrollableScrollPhysics(),
          children: categories.map((category) {
            return StaggeredGridView.countBuilder(
              crossAxisCount: 2,
              itemCount: 10,
              crossAxisSpacing: 10,
              mainAxisSpacing: 16,
              staggeredTileBuilder: (index) => StaggeredTile.fit(1),
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 16,
                    right: 16,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      color: Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Shimmer.fromColors(
                          baseColor: Colors.grey[300],
                          highlightColor: Colors.grey[200],
                          enabled: true,
                          child: Container(
                            width: Get.width,
                            height: 120,
                            color: Colors.white,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: 10,
                            top: 10,
                            right: 10,
                            bottom: 5,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Shimmer.fromColors(
                                baseColor: Colors.grey[300],
                                highlightColor: Colors.grey[200],
                                enabled: true,
                                child: Container(
                                  width: 80,
                                  height: 20,
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(height: 10,),
                              Row(
                                children: [
                                  Expanded(
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.grey[300],
                                      highlightColor: Colors.grey[200],
                                      enabled: true,
                                      child: Container(
                                        height: 20,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          }).toList(),
        ),
      ),
    );
  }
}
