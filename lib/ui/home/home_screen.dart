import 'package:badges/badges.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/home/widgets/categories_widget.dart';
import 'package:bay_of_biriyani/ui/home/widgets/header_widget.dart';
import 'package:bay_of_biriyani/ui/home/widgets/home_shimmer.dart';
import 'package:bay_of_biriyani/ui/main/controller/main_controller.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'controller/foods_controller.dart';
import 'widgets/greetings_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GetStorage _storage = GetStorage();
  MainController mainController = Get.find();
  FoodsController foodsController = Get.find();

  @override
  void initState() {
    super.initState();
    mainController.getAddressFromCoords();
    // var location = _storage.read(Constants.USER_LOCATION);
    // location['latitude'], location['longitude']

    foodsController.onFetchHome("123", "123");
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        backgroundColor: Themes.backgroundColor,
        // body: CategoriesWidget(
        //   categories: catList,
        // ),
        body: Obx(() {
          print(mainController.locationAddress.value);
          if (mainController.locationAddress.value != null) {
            var location = _storage.read(Constants.USER_LOCATION);
            foodsController.onFetchHome(location['latitude'].toString(), location['longitude'].toString());
            return Obx(() => foodsController.dataAvailable
                ? CategoriesWidget(
                    data: foodsController.responseData,
                  )
                : HomeShimmer());
          } else {
            return HomeShimmer();
          }
        }),
      ),
    );
  }
}
