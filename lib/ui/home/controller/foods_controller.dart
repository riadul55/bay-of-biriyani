import 'package:bay_of_biriyani/api/api.dart';
import 'package:bay_of_biriyani/api/body/api_body.dart';
import 'package:bay_of_biriyani/api/data_models/home_data_model.dart';
import 'package:get/get.dart';

class FoodsController extends GetxController {
  var _dataAvailable = false.obs;
  var _data;

  bool get dataAvailable => _dataAvailable.value;

  HomeDataModel get responseData => _data;

  void onFetchHome(String lat, String lon) {
    Api.instance
        .getHomeData(body: ApiBody.getHomeBody(lat: lat, lon: lon))
        .then((value) {
          print(value.toJson());
      if (value != null) {
        _data = value;
      }
    }).onError((error, stackTrace) {
      print(error.toString());
    }).whenComplete(() => _dataAvailable.value = _data != null);
  }

  @override
  void onInit() {
    super.onInit();
  }
}
