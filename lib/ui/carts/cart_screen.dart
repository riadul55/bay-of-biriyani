import 'package:badges/badges.dart';
import 'package:bay_of_biriyani/ui/carts/place_order_screen.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../main/widgets/custom_app_bar.dart';
import 'widgets/cart_apply_promo.dart';
import 'widgets/cart_footer.dart';
import 'widgets/cart_list.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: CustomAppBar2(),
        ),
        body: Container(
          color: Themes.backgroundColor,
          padding: EdgeInsets.only(right: 16, top: 16),
          child: ListView(
            children: [
              CartList(),
              CartApplyPromo(),
              CartFooter(),
            ],
          ),
        ),
      ),
    );
  }
}
