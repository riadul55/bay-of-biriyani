import 'dart:convert';

import 'package:bay_of_biriyani/api/api.dart';
import 'package:bay_of_biriyani/api/body/api_body.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/location/map_screen.dart';
import 'package:bay_of_biriyani/ui/main/controller/main_controller.dart';
import 'package:bay_of_biriyani/ui/orders/controller/order_controller.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sslcommerze/model/SSLCCustomerInfoInitializer.dart';
import 'package:flutter_sslcommerze/model/SSLCSdkType.dart';
import 'package:flutter_sslcommerze/model/SSLCShipmentInfoInitializer.dart';
import 'package:flutter_sslcommerze/model/SSLCommerzInitialization.dart';
import 'package:flutter_sslcommerze/model/SSLCurrencyType.dart';
import 'package:flutter_sslcommerze/sslcommerz.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

enum PaymentMethods { cash, online }

class PlaceOrderScreen extends StatefulWidget {
  @override
  _PlaceOrderScreenState createState() => _PlaceOrderScreenState();
}

class _PlaceOrderScreenState extends State<PlaceOrderScreen> {
  MainController mainController = Get.find();
  PaymentMethods _methods = PaymentMethods.cash;

  String phoneNumber = "01717849968";
  String fullName = "registration";

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.primaryColor,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Themes.backgroundColor,
          leading: Padding(
            padding: EdgeInsets.only(left: 16),
            child: CircleAvatar(
              backgroundColor: Themes.accentColor,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ),
          title: Text("Payment"),
        ),
        body: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: 20,
                ),
                ListTile(
                  leading: Container(
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      color: Themes.primaryColor,
                      image: DecorationImage(
                        image: AssetImage(Constants.LOCATION_DEMO_IMAGE),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  title: Text(
                    "Deliver To:",
                    style: Themes.textStyle(
                      color: Themes.primaryText,
                      weight: FontWeight.w600,
                    ),
                  ),
                  subtitle: Obx(() => Text(mainController.locationAddress.value ?? "")),
                  trailing: TextButton(
                    child: Text(
                      "Change",
                      style: TextStyle(color: Themes.accentColor),
                    ),
                    onPressed: () async {
                      await Get.to(() => MapScreen());
                    },
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: 16,
                    bottom: 10,
                  ),
                  child: Text(
                    "Payment Method",
                    style: Themes.textStyle(
                      size: 20,
                      color: Themes.primaryText,
                      weight: FontWeight.w600,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ListTile(
                    selected: _methods.index == 0,
                    selectedTileColor: Themes.primaryColor,
                    title: Text(
                      "Cash On Delivery",
                      style: Themes.textStyle(
                        color: Themes.primaryText,
                        weight: FontWeight.w500,
                      ),
                    ),
                    trailing: Radio<PaymentMethods>(
                      activeColor: Themes.accentColor,
                      value: PaymentMethods.cash,
                      groupValue: _methods,
                      onChanged: (PaymentMethods value) {
                        setState(() {
                          _methods = value;
                        });
                      },
                    ),
                    onTap: () {
                      setState(() {
                        _methods = PaymentMethods.cash;
                      });
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ListTile(
                    selected: _methods.index == 1,
                    selectedTileColor: Themes.primaryColor,
                    title: Text(
                      "Digital Payment",
                      style: Themes.textStyle(
                        color: Themes.primaryText,
                        weight: FontWeight.w500,
                      ),
                    ),
                    trailing: Radio<PaymentMethods>(
                      activeColor: Themes.accentColor,
                      value: PaymentMethods.online,
                      groupValue: _methods,
                      onChanged: (PaymentMethods value) {
                        setState(() {
                          _methods = value;
                        });
                      },
                    ),
                    onTap: () {
                      setState(() {
                        _methods = PaymentMethods.online;
                      });
                    },
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 20,
              left: 20,
              right: 20,
              child: Container(
                width: Get.width,
                child: isLoading
                    ? Center(
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Themes.accentColor),
                        ),
                      )
                    : SubmitBtnWidgets(
                        text: "Place Order",
                        bgColor: Themes.accentColor,
                        borderColor: Themes.accentColor,
                        radius: 4.0,
                        padding: EdgeInsets.all(10),
                        onSubmit: () async {
                          if (isValid()) {
                            if (_methods == PaymentMethods.cash) {
                              cashOnDeliveryCall();
                            } else {
                              sslCommerzGeneralCall();
                            }
                          }
                        },
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> cashOnDeliveryCall() async {
    setState(() => isLoading = true);
    var json = jsonEncode(
        CartController.instance.cartList.map((e) => e.toJson()).toList());
    print(json.toString());
    await Api.instance
        .getOrderSaveData(
        body: ApiBody.getOrderSaveBody(
          phoneNumber: phoneNumber,
          fullName: fullName,
          address: mainController.locationAddress.value,
          orderItems: json,
          deliveryCharge: CartController.instance.deliveryCharge,
          promoValue: CartController.instance.promoDiscount,
          promoCode: CartController.instance.promoCode.value ?? "TEST10",
          paymentType: _methods == PaymentMethods.cash ? "cash" : "online",
        ))
        .then((value) {
      if (value.statusCode == 200) {
        CartController.instance.clearCart();
        OrderController.instance.clearOrderHistory();
        Get.back();
        Get.snackbar(
          "Success",
          value.message,
          backgroundColor: Colors.green,
          colorText: Colors.white,
          duration: Duration(seconds: 3),
        );
      } else {
        print(value.toJson().toString());
        Get.snackbar(
          "Result",
          value.message,
          backgroundColor: Colors.red,
          colorText: Colors.white,
          duration: Duration(seconds: 3),
        );
      }
    }).catchError((onError) {
      print(onError);
      Get.snackbar(
        "Error",
        "Something went wrong, Please try again later!",
        backgroundColor: Colors.red,
        colorText: Colors.white,
        duration: Duration(seconds: 3),
      );
    });
    setState(() => isLoading = false);
  }

  Future<void> sslCommerzGeneralCall() async {
    Sslcommerz sslcommerz = Sslcommerz(
        initializer: SSLCommerzInitialization(
            //Use the ipn if you have valid one, or it will fail the transaction.
            //   ipn_url: "food.eudocode.com",
            multi_card_name: "master",
            currency: SSLCurrencyType.BDT,
            product_category: "Food",
            sdkType: SSLCSdkType.TESTBOX,
            store_id: Constants.STORE_ID,
            store_passwd: Constants.STORE_PASSWORD,
            total_amount: CartController.instance.grandTotal,
            tran_id: "1231321321321312"));
    sslcommerz.addCustomerInfoInitializer(
        customerInfoInitializer: SSLCCustomerInfoInitializer(
            customerState: "Dhaka",
            customerName: "Abu Sayed Chowdhury",
            customerEmail: null,
            customerAddress1: "Gulshan",
            customerCity: "Dhaka",
            customerPostCode: "1212",
            customerCountry: "Bangladesh",
            customerPhone: phoneNumber));
    var result = await sslcommerz.payNow();
    if (result is PlatformException) {
      Get.snackbar(
        "SSL Error code : ${result.code}",
        result.message.toString(),
        backgroundColor: Colors.red,
        colorText: Colors.white,
        duration: Duration(seconds: 3),
      );
    } else {
      setState(() => isLoading = true);
      var json = jsonEncode(
          CartController.instance.cartList.map((e) => e.toJson()).toList());
      await Api.instance
          .getOrderSaveData(
              body: ApiBody.getOrderSaveBody(
        phoneNumber: phoneNumber,
        fullName: fullName,
        address: mainController.locationAddress.value,
        orderItems: json,
        deliveryCharge: CartController.instance.deliveryCharge,
        promoValue: CartController.instance.promoDiscount,
        promoCode: CartController.instance.promoCode.value ?? "DHK100",
        paymentType: _methods == PaymentMethods.cash ? "cash" : "online",
      ))
          .then((value) {
        if (value.statusCode == 200) {
          CartController.instance.clearCart();
          OrderController.instance.clearOrderHistory();
          Get.back();
          Get.snackbar(
            "Success",
            value.message,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            duration: Duration(seconds: 3),
          );
          sslCommerzGeneralCall();
        } else {
          print(value.toJson().toString());
          Get.snackbar(
            "Result",
            value.message,
            backgroundColor: Colors.red,
            colorText: Colors.white,
            duration: Duration(seconds: 3),
          );
        }
      }).catchError((onError) {
        print(onError);
        Get.snackbar(
          "Error",
          "Something went wrong, Please try again later!",
          backgroundColor: Colors.red,
          colorText: Colors.white,
          duration: Duration(seconds: 3),
        );
      });
      setState(() => isLoading = false);
    }
  }

  bool isValid() {
    if (phoneNumber == null || phoneNumber.isEmpty) {
      phoneNumber = "01717849968";
      // Get.snackbar("Warning", "Need to login first",backgroundColor: Themes.accentColor, colorText: Colors.white);
      return true;
    }
    return true;
  }
}
