import 'package:bay_of_biriyani/ui/widgets/input_widgets.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'apply_promo_dialog.dart';

class CartApplyPromo extends StatefulWidget {
  @override
  _CartApplyPromoState createState() => _CartApplyPromoState();
}

class _CartApplyPromoState extends State<CartApplyPromo> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: 16,
        top: 20,
      ),
      decoration: BoxDecoration(
        color: Themes.primaryColor,
      ),
      child: TextButton(
        child: Container(
          padding: EdgeInsets.all(5),
          child: Row(
            children: [
              Icon(
                Icons.sticky_note_2_outlined,
                size: 30,
                color: Themes.accentColor,
              ),
              SizedBox(
                width: 20,
              ),
              Text(
                "Apply Promo",
                style: Themes.textStyle(
                  size: 20,
                  color: Themes.accentColor,
                ),
              ),
            ],
          ),
        ),
        onPressed: () => showDialog(
          context: context,
          builder: (context) {
            return ApplyPromoDialog();
          },
        ),
      ),
    );
  }
}
