import 'package:bay_of_biriyani/local_db/AppDb.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/widgets/custom_network_image.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartList extends StatefulWidget {
  @override
  _CartListState createState() => _CartListState();
}

class _CartListState extends State<CartList> {
  @override
  Widget build(BuildContext context) {
    return GetX<CartController>(builder: (controller) {
      return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: controller.cartList.length,
        itemBuilder: (context, position) {
          FoodsTable food = controller.cartList[position];
          return Container(
            // margin: EdgeInsets.only(bottom: 16,),
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 20),
                  decoration: BoxDecoration(
                      color: Themes.backgroundColor,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Row(
                    children: <Widget>[
                      // Container(
                      //   margin: EdgeInsets.only(
                      //       right: 8, left: 8, top: 8, bottom: 8),
                      //   width: 80,
                      //   height: 80,
                      //   decoration: BoxDecoration(
                      //     borderRadius: BorderRadius.all(Radius.circular(10)),
                      //     color: Themes.primaryColor,
                      //     image: DecorationImage(
                      //       image: NetworkImage("${Constants.BASE_URL_IMAGE}${food.image}",),
                      //       fit: BoxFit.cover,
                      //     ),
                      //   ),
                      // ),
                      CustomNetworkImage(
                        image: food.image,
                        width: 80,
                        height: 80,
                      ),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(right: 8, top: 4),
                                child: Text(
                                  "${food.name}",
                                  maxLines: 2,
                                  softWrap: true,
                                  overflow: TextOverflow.ellipsis,
                                  style: Themes.textStyle(
                                    size: 18,
                                    color: Themes.primaryText,
                                    weight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "\$${food.price}",
                                      style: Themes.textStyle(
                                        size: 18,
                                        color: Themes.accentColor,
                                        weight: FontWeight.w500,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          IconButton(
                                              icon: Icon(
                                                Icons.remove,
                                                size: 24,
                                                color: Colors.grey.shade700,
                                              ),
                                              onPressed: () {
                                                if (food.quantity > 1) {
                                                  int q = food.quantity - 1;
                                                  print(q);
                                                  CartController.instance
                                                      .updateCart(food.id, q);
                                                } else {
                                                  showAlert(
                                                      context, food.foodId);
                                                }
                                              }),
                                          Container(
                                            color: Colors.grey.shade200,
                                            padding: const EdgeInsets.only(
                                              bottom: 10,
                                              right: 12,
                                              left: 12,
                                              top: 10,
                                            ),
                                            child: Text(
                                              "${food.quantity}",
                                            ),
                                          ),
                                          IconButton(
                                              icon: Icon(
                                                Icons.add,
                                                size: 24,
                                                color: Colors.grey.shade700,
                                              ),
                                              onPressed: () {
                                                if (food.quantity < 10) {
                                                  int q = food.quantity + 1;
                                                  CartController.instance
                                                      .updateCart(food.id, q);
                                                } else {
                                                  Get.snackbar(
                                                    "Warning",
                                                    "You can't add more then 10",
                                                    backgroundColor:
                                                        Themes.primaryColor,
                                                    colorText:
                                                        Themes.primaryText,
                                                  );
                                                }
                                              })
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        flex: 100,
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: () {
                      showAlert(context, food.foodId);
                    },
                    child: Container(
                      width: 24,
                      height: 24,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(right: 10, top: 10),
                      child: Icon(
                        Icons.close,
                        color: Colors.white,
                        size: 20,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        color: Themes.accentColor,
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        },
      );
    });
  }

  showAlert(BuildContext context, foodId) {
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Get.back();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Delete"),
      onPressed: () {
        CartController.instance.removeCart(foodId);
        Get.back();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content: Text("Would you like to remove it from Cart?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
