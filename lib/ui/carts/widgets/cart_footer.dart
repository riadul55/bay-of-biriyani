import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/carts/place_order_screen.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartFooter extends StatelessWidget {
  var payments_info_style = Themes.textStyle(
    color: Themes.primaryText,
  );

  @override
  Widget build(BuildContext context) {
    return GetX<CartController>(builder: (controller) {
      return Container(
        margin: EdgeInsets.only(
          left: 16,
          top: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text(
                "Payment",
                style: Themes.textStyle(
                  size: 20,
                  color: Themes.primaryText,
                  weight: FontWeight.w500,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16, left: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Item total",
                    style: payments_info_style,
                  ),
                  Text(
                    "Tk ${controller.totalPrice}",
                    style: payments_info_style,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(
                right: 16,
                left: 16,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Delivery fee",
                    style: payments_info_style,
                  ),
                  Text(
                    "Tk ${controller.deliveryCharge}",
                    style: payments_info_style,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(
                right: 16,
                left: 16,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Discount",
                    style: payments_info_style,
                  ),
                  Text(
                    "Tk ${controller.promoDiscount}",
                    style: payments_info_style,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(
                right: 16,
                left: 16,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "To Pay",
                    style: Themes.textStyle(
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    "Tk ${controller.grandTotal}",
                    style: Themes.textStyle(
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              width: Get.width,
              child: SubmitBtnWidgets(
                text: "Check Out",
                bgColor: Themes.accentColor,
                borderColor: Themes.accentColor,
                radius: 4.0,
                padding: EdgeInsets.all(10),
                onSubmit: () {
                  Get.to(() => PlaceOrderScreen());
                },
              ),
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      );
    });
  }
}
