import 'package:bay_of_biriyani/api/api.dart';
import 'package:bay_of_biriyani/api/body/api_body.dart';
import 'package:bay_of_biriyani/ui/carts/controller/cart_controller.dart';
import 'package:bay_of_biriyani/ui/widgets/input_widgets.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ApplyPromoDialog extends StatefulWidget {
  ApplyPromoDialog({Key key}) : super(key: key);

  @override
  _ApplyPromoDialogState createState() => _ApplyPromoDialogState();
}

class _ApplyPromoDialogState extends State<ApplyPromoDialog> {
  String editTextValue = "";
  String errorMessage;
  bool promoCodeIsValid = true;

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Apply Promo"),
      content: Wrap(
        children: [
          InputWidget(
            validate: promoCodeIsValid,
            errorText: errorMessage,
            onChanged: (value) {
              setState(() {
                promoCodeIsValid = true;
                editTextValue = value;
              });
            },
          ),
          isLoading
              ? LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Themes.accentColor),
                  backgroundColor: Themes.backgroundColor,
                )
              : Container(),
        ],
      ),
      actions: [
        TextButton(
          child: Text(
            "Cancel",
            style: TextStyle(color: Themes.accentColor),
          ),
          onPressed: () => Navigator.pop(context),
        ),
        TextButton(
          child: Text(
            "Apply",
            style: TextStyle(color: Themes.accentColor),
          ),
          onPressed: () async {
            if (editTextValue.isNotEmpty) {
              setState(() => isLoading = true);
              await Api.instance
                  .getPromoData(
                body: ApiBody.getPromoBody(
                  promoCode: editTextValue,
                  minSpend: CartController.instance.totalPrice,
                ),
              )
                  .then((value) {
                if (value.statusCode == 200) {
                  CartController.instance
                      .addDiscount(value.discount.toDouble(), editTextValue);
                  Get.back();
                  Get.snackbar(
                    "Success",
                    value.message,
                    backgroundColor: Colors.green,
                    colorText: Colors.white,
                    duration: Duration(seconds: 3),
                  );
                } else {
                  showError(message: value.message);
                }
              }).onError((error, stackTrace) {
                print(error);
                showError(
                    message: "Something went wrong, Please try again later!");
              });
              setState(() => isLoading = false);
            } else {
              showError(message: "Empty value can't apply");
            }
          },
        ),
      ],
    );
  }

  showError({message}) {
    setState(() {
      promoCodeIsValid = false;
      errorMessage = message;
    });
  }
}
