import 'package:bay_of_biriyani/api/data_models/home_data_model.dart';
import 'package:bay_of_biriyani/local_db/AppDb.dart';
import 'package:bay_of_biriyani/local_db/repositories/db_repositories.dart';
import 'package:get/get.dart';

class CartController extends GetxController {
  static CartController instance = Get.find();

  var cartList = [].obs;
  var discount = 0.0.obs;
  var promoCode = "".obs;

  double get promoDiscount => discount.value;
  double get totalPrice => cartList.fold(0, (sum, item) => sum + (double.parse(item.price) * item.quantity));
  double get deliveryCharge => cartList.isNotEmpty ? double.parse(cartList.first.charge) : 0.0;
  double get grandTotal => (cartList.fold(0, (sum, item) => sum + (double.parse(item.price) * item.quantity)) + deliveryCharge) - promoDiscount;

  DbRepositories repositories = DbRepositories();

  @override
  void onInit() {
    super.onInit();
    getAllCart();
  }

  addToCart(FoodsTable table) {
    repositories.addCart(table);
    getAllCart();
  }

  removeCart(foodId) {
    repositories.deleteCartById(foodId);
    getAllCart();
  }

  updateCart(id, quantity) {
    repositories.updateCartById(id, quantity);
    getAllCart();
  }

  bool isCartAdded(FoodItems food){
    return cartList.any((element) => element.foodId == food.id);
  }

  clearCart() {
    repositories.deleteAllCarts();
    getAllCart();
  }

  getAllCart() {
    repositories.getAllCarts().then((List<FoodsTable> value) {
      cartList.value = value;

      print(cartList.value);
    }).catchError((onError) {
      cartList.value = null;
    });
    update();
  }

  addDiscount(double discount, String code) {
    this.discount(discount);
    this.promoCode(code);
  }

}