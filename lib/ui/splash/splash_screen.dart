import 'dart:async';

import 'package:bay_of_biriyani/ui/location/geo_screen.dart';
import 'package:bay_of_biriyani/ui/main/main_screen.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:get_storage/get_storage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  GetStorage boxStorage = GetStorage();

  @override
  void initState() {
    super.initState();

    Timer(
      Duration(seconds: 3),
      () {
        Get.off(
          () {
            if (boxStorage.hasData(Constants.USER_LOCATION)) {
              return MainScreen();
            } else {
              return GeoScreen();
            }
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Themes.backgroundColor,
      body: Center(
        child: Container(
          width: Get.width,
          padding: EdgeInsets.all(50),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                Constants.APP_LOGO,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
