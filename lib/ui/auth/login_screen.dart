import 'package:bay_of_biriyani/ui/auth/otp_screen.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/ui/widgets/input_widgets.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.accentColor,
      child: Scaffold(
        backgroundColor: Themes.backgroundColor,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: Get.width,
                  height: 200,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(Constants.APP_LOGO),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                InputWidget(
                  labelText: "Phone Number",
                  onChanged: (value) {},
                ),
                // SizedBox(
                //   height: 20,
                // ),
                // InputWidget(
                //   labelText: "Password",
                //   obscureText: true,
                //   onChanged: (value) {},
                // ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  width: Get.width,
                  child: SubmitBtnWidgets(
                    text: "Log In",
                    bgColor: Themes.accentColor,
                    borderColor: Themes.accentColor,
                    radius: 4.0,
                    padding: EdgeInsets.all(10),
                    onSubmit: () {
                      Get.to(() => OtpScreen());
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
