import 'dart:async';

import 'package:bay_of_biriyani/ui/main/main_screen.dart';
import 'package:bay_of_biriyani/ui/widgets/background_widget.dart';
import 'package:bay_of_biriyani/ui/widgets/input_widgets.dart';
import 'package:bay_of_biriyani/ui/widgets/sumit_btn_widgets.dart';
import 'package:bay_of_biriyani/utils/constants.dart';
import 'package:bay_of_biriyani/utils/themes.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class OtpScreen extends StatefulWidget {
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  TextEditingController textEditingController = TextEditingController();

  // ..text = "123456";

  StreamController<ErrorAnimationType> errorController;

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      statusBar: Themes.accentColor,
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: Themes.backgroundColor,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Container(
                  width: Get.width,
                  height: 200,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(Constants.APP_LOGO),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Form(
                  key: formKey,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 30),
                      child: PinCodeTextField(
                        appContext: context,
                        pastedTextStyle: TextStyle(
                          color: Themes.primaryText,
                          fontWeight: FontWeight.bold,
                        ),
                        length: 4,
                        obscureText: false,
                        obscuringCharacter: '*',
                        animationType: AnimationType.fade,
                        validator: (v) {
                          if (v.length < 3) {
                            return null;
                          } else {
                            return null;
                          }
                        },
                        pinTheme: PinTheme(
                          selectedColor: Themes.primaryColor,
                          inactiveColor: Themes.primaryColor,
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(5),
                          fieldHeight: (Get.width - 100) / 4,
                          fieldWidth: (Get.width - 100) / 4,
                          activeFillColor: Themes.primaryColor,
                          activeColor: hasError ? Colors.green : Colors.red,
                        ),
                        cursorColor: Themes.primaryText,
                        animationDuration: Duration(milliseconds: 300),
                        textStyle: TextStyle(
                          fontSize: 20,
                          height: 1.6,
                          color: Themes.primaryText,
                        ),
                        backgroundColor: Colors.white,
                        enableActiveFill: false,
                        errorAnimationController: errorController,
                        controller: textEditingController,
                        keyboardType: TextInputType.number,
                        autoFocus: true,
                        boxShadows: [
                          BoxShadow(
                            offset: Offset(0, 1),
                            color: Themes.primaryColor,
                            blurRadius: 10,
                          )
                        ],
                        onCompleted: (v) {
                          print("Completed" + v.toString());
                        },
                        onChanged: (value) {
                          print(value);
                          setState(() {
                            currentText = value;
                          });
                        },
                        beforeTextPaste: (text) {
                          print("Allowing to paste $text");
                          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                          //but you can show anything you want here, like your pop up saying wrong paste format or etc
                          return true;
                        },
                      )),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  width: Get.width,
                  child: SubmitBtnWidgets(
                    text: "Verify",
                    bgColor: Themes.accentColor,
                    borderColor: Themes.accentColor,
                    radius: 4.0,
                    padding: EdgeInsets.all(10),
                    onSubmit: () {
                      // setState(() {
                      //   if(hasError) {
                      //     hasError = false;
                      //   } else {
                      //     hasError = true;
                      //   }
                      // });
                      Get.offAll(() => MainScreen());
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
